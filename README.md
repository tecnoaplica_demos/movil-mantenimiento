# Mantenimiento - Mobile Application

- Mobile Application (Ionic + Gale + Cordova)
- API (Gale RESTful)


## Resources 

- [Angular Gale Doc's](http://angular-gale.azurewebsites.net/)
- [Framework API](http://gale.azurewebsites.net/)

## Get Started

### Get dependencies

```sh
# bash
npm install				# restore npm packages
ionic state restore		# restore ionic plugin's
bower install			# restore bower libraries
```

### Mount development server

```sh
# bash
ionic serve -d			# Mount web server without [livereload]
```

### Build for target device's

```sh
# bash
ionic build	ios			# build for ios (only for mac)
ionic build	android		# build for android
```

## Screenshots

![](./screenshots/demo.gif)
![](./screenshots/ss-1.jpg)
![](./screenshots/ss-2.jpg)
![](./screenshots/ss-3.jpg)
![](./screenshots/ss-4.jpg)
![](./screenshots/ss-5.jpg)
![](./screenshots/ss-6.jpg)