angular.route('blank.firstRun/configure/index', function(
    $scope,
    $location,
    $log,
    $Configuration,
    $ionicLoading,
    $LocalStorage,
    $Localization,
    $timeout,
    $q
) {
    //---------------------------------------------------
    //New Slider (Ionic 1.2)
    var slider = null;
    $scope.$watch("slider", function(value) {
        if (value) {
            slider = value;
        }
    });
    //---------------------------------------------------

    var save = function() {
        $ionicLoading.show({
            template: $Localization.get("LABEL.SAVING") + "...",
        });


        var defer = $q.defer();

        //Save configuration
        var configuration = { configured: true };
        var label = $Configuration.get("stamps").personal_data;
        $LocalStorage.setObject(label, configuration);

        var delay = $timeout(function() {
            $timeout.cancel(delay);

            defer.resolve(configuration);
        }, 100);


        var finaly = function() {
            $ionicLoading.hide();
        };
        defer.promise.then(finaly, finaly);

        return defer.promise;
    };

    $scope.next = function(id) {
        switch (id) {
            case "PUSH_NOTIFICATIONS":
                slider.slideNext();
                break;
            case "GPS_TRACKER":
                save().then(function(conf) {
                    var conf = $Configuration.get("application");
                    $location.url(conf.home);
                });
                break;
        }
    };
});
