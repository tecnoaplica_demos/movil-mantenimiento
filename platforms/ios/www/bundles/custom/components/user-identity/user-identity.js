angular.module("custom.controllers")
    .directive('userIdentity', function() {
        return {
            restrict: 'E',
            scope:  {
                onAuthenticated: '&', //Complete Step
                onCancel: '&', //Close Step
                canCancel: '=', //Can Cancel??
                canRegister: '=', //Can Register??
                canRecoverPassword: '=', //Can Recover Password??
                authenticators: '=' //Array of Availables Authenticators
            },
            templateUrl: 'bundles/custom/components/user-identity/user-identity.tpl.html',
            controller: function($scope, $Api, $q, $Identity, $ionicLoading) {

                //---------------------------------------------------
                // SETTINGS (DEFAULT'S AND MERGE)              
                var settings = {
                    onAuthenticated: null,
                    onCancel: null,
                    canCancel: true,
                    canRegister: false,
                    canRecoverPassword: false,
                    authenticators: []
                };

                //Merge from developer option's
                for (var name in settings) {
                    if (!angular.isUndefined($scope[name])) {
                        settings[name] = $scope[name];
                    }
                };
                $scope.settings = settings;
                //---------------------------------------------------

                //---------------------------------------------------
                // INITIALIZATION
                var slides = {
                    USER_LOGIN: 0,
                    REGISTER: 1,
                    CHANGE_PASSWORD: 2
                };

                //If is authenticated send the trigger to change the scope!
                $scope.identity = $Identity;
                if ($Identity.isAuthenticated()) {
                    $scope.onAuthenticated();
                    return;
                }

                //New Slider (Ionic 1.2)
                var initialSlide = slides.USER_LOGIN;
                $scope.swiper = {
                    instance: null,
                    options: {
                        initialSlide: initialSlide,
                        /* Initialize a scope variable with the swiper */
                        onInit: function(swiper) {
                            $scope.swiper.instance = swiper;
                        }
                    }
                };
                //---------------------------------------------------

                //---------------------------------------------------
                // Action's
                $scope.close = function() {
                    $scope.onCancel();
                };

                $scope.actionCall = function(action, argument) {
                    switch (action) {
                        case "REGISTER":
                            break;
                        case "AUTHENTICATED":
                            $scope.onAuthenticated();
                            break;
                        case "CLOSE":
                            $scope.close();
                            break;
                    }
                };
                //---------------------------------------------------

            }
        };
    });
