angular.route('app.activities/create/index/:activity/:component', function(
    $scope,
    $state,
    $log,
    $Api,
    $timeout,
    $stateParams,
    $Identity,
    $ionicLoading,
    $Localization,
    $ionicActionSheet,
    $q,
    Camera
) {
    //------------------------------------------------
    // Model
    $scope.model = {};
    $scope.data = {};

    var defers = [
        $Api.read("Activities/{activity}", {
            activity: $stateParams.activity
        })
    ];

    //------------------------------------------------    
    $q.all(defers).then(function(resolves) {
        var workOrder = resolves[0];


        //SET MODEL 
        $scope.model = workOrder;

        $log.debug(workOrder);
    });

    //------------------------------------------------
    // Action's
    $scope.back = function() {
        $state.go("app.components/view/index", {
            component: $stateParams.component
        });
    };

    $scope.takePicture = function() {
        Camera.takePicture().then(function(imageData) {
            $scope.data.image = imageData;
        });
    };

    $scope.save = function(activities) {

        // Show the action sheet
        $ionicActionSheet.show({
            titleText: '¿Cerrar Orden de Trabajo?',
            destructiveText: 'Sí, Cerrar Orden',
            cancelText: 'Cancelar',
            cancel: function() {
                return true;
            },
            destructiveButtonClicked: function() {
                $ionicLoading.show({
                    template: $Localization.get("LABEL.SAVING") + "...",
                });

                var selecteds = _.where(activities, {
                    selected: true
                });

                var data = $scope.data;
                data.selecteds = _.pluck(selecteds, 'token');

                //TRACK TO SERVER THE GEO-LOCATION
                $Api.create("Activities/{activity}", {
                        activity: $stateParams.activity,
                        data: data
                    })
                    .success(function(data) {
                        $scope.back();
                    })
                    .finally($ionicLoading.hide);

                return true;
            }
        });

    };


});
