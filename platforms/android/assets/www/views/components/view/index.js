angular.route('app.components/view/index/:component', function(
    $scope,
    $state,
    $log,
    $Api,
    $timeout,
    $stateParams,
    $Identity,
    $ionicLoading,
    BeaconFinder,
    $Localization,
    $ionicActionSheet,
    $q
) {
    //------------------------------------------------
    // Model
    $scope.model = {};

    var defers = [
        $Api.read("Activities/Components/{component}", {
            component: $stateParams.component
        })
    ];

    //------------------------------------------------    
    $q.all(defers).then(function(resolves) {
        var component = resolves[0];

        //Flag to check the near of the component
        component.isNear = true;

        var chart = {
            labels: [],
            data: [],
            colours: []
        };
        angular.forEach(component.statistics, function(item) {
            chart.labels.push(item.label);
            chart.data.push(item.value);
            chart.colours.push(item.color);
        });

        //Set Model
        $scope.model.component = component;


        //SET COLOR TO EACH ACTIVITY ACCORD OF HIS GENERAL STATE
        angular.forEach(component.activities, function(activity) {
            var state = _.find(component.statistics, {
                identifier: activity.groupedState
            });
            if (state) {
                activity.groupedColor = state.color;
            }
        });

        //SET CHART
        $scope.model.chart = {
            labels: chart.labels,
            data: chart.data,
            colours: chart.colours,
            options: {
                percentageInnerCutout: 60,
                segmentStrokeWidth: 3,
                responsive: false,
                maintainAspectRatio: false
            }
        };

        if (!component.beacon) {
            $log.error("BEACON IS NOT CONFIGURED");
        } else {
            BeaconFinder.scan(onScan, onError);
        }

        $log.debug(component);
    });

    //------------------------------------------------
    // Action's
    $scope.back = function() {
        $state.go("app.home/index");
    };

    $scope.details = function(item) {
        var onComplete = function() {
            $state.go("app.activities/create/index", {
                activity: item.token,
                component: $stateParams.component
            });
        };


        if (item.groupedState == 'REAL') {
            return;
        }

        if (item.groupedState == 'ENPR') {
            onComplete();
            return;
        }
        // Show the action sheet
        $ionicActionSheet.show({
            titleText: '¿Iniciar Actividad?',
            destructiveText: 'Activar Orden de Trabajo',
            cancelText: 'Cancelar',
            cancel: function() {
                return true;
            },
            destructiveButtonClicked: function() {
                $ionicLoading.show({
                    template: $Localization.get("LABEL.SAVING") + "...",
                });

                //TRACK TO SERVER THE GEO-LOCATION
                $Api.update("Activities/{activity}/Activate", {
                        activity: item.token
                    })
                    .success(function(data) {
                        onComplete();
                    })
                    .finally($ionicLoading.hide);

                return true;
            }
        });
    };


    //------------------------------------------------
    // Beacon's
    var onScan = function(beacons) {
        var beacon = $scope.model.component; //BEACON KEY
        var threshold = 1; //meter max??


        //The beacon is in the monitoring beacon??
        var findedBeacon = _.find(beacons, {
            key: beacon.beacon
        });

        //Match??
        if (findedBeacon) {

            if (findedBeacon.distance > threshold) {
               beacon.isNear = true;
            }
            //Update the distance to the "core" manager 
            equipedBeacon.distance = findedBeacon.distance;

            console.log("finded:", findedBeacon);
        }



    };

    var onError = function(err) {
        $log.error(err);
    };
    $scope.$on('$destroy', function() {
        console.log("stopping scan...")
        BeaconFinder.stop();
    });

});
