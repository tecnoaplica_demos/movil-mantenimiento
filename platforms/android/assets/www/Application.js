﻿/*

    gale:                   ANGULAR-GALE LIBRARY
    ionic:                  IONIC SDK
    app:                    APP PROJECT LIBRARY
    custom:                 CUSTOM COMPONENTS
    mocks:                  MOCKS ONLY FOR TESTING
    ngCordova:              CORDOVA LIBRARY
    pouchdb                 POUCH DB (FOR DATA STORAGE)
                            Load (Dump File) Plugin: https://github.com/nolanlawson/pouchdb-load
                            Find Plugin: https://github.com/nolanlawson/pouchdb-find
                            Quick Search Plugin: https://github.com/nolanlawson/pouchdb-quick-search#autosuggestions-and-prefix-search

    angularMoment:          ANGULAR MOMENT JS
    ngIOS9UIWebViewPatch:   IOS 9 FICKLERING PATCH (https://gist.github.com/IgorMinar/863acd413e3925bf282c)
    chart.js:               ANGULAR CHART'S (http://jtblin.github.io/angular-chart.js/)
                                CHART OPTIONS: http://carlcraig.github.io/tc-angular-chartjs/doughnut/

*/
angular.module('App', [
        'gale',
        'ionic',

        'app',
        'custom',
        

        'ngCordova',
        'pouchdb',
        'angularMoment',
        'ngIOS9UIWebViewPatch',
        'chart.js'
    ])
    .run(function($location, $Configuration, $log) {

        //REDIRECT TO MAIN HOME (ONLY WHEN NO HAVE PATH)
        var currentPath = $location.url();
        var boot = $location.path("boot").search({
            path: currentPath
        });

        $location.url(boot.url());

    })
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });
    })
    /*
        .config(function(ChartJsProvider) {
            ChartJsProvider.setOptions({
                colours: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
            });
        })*/
    .config(function($cordovaFacebookProvider) {
        //Faceboook Login , -> Native Support, otherwise browser
        if (!ionic.Platform.isWebView()) {
            var init = function() {
                var appID = 1624612227852176;
                var version = "v2.6";
                $cordovaFacebookProvider.browserInit(appID, version);
            };

            //if FB (Facebook Plugin)
            if (typeof FB !== "undefined") {
                init();
            } else {
                //Wait for the FB
                window.fbAsyncInit = init;
            }

        }
    })
    .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.views.swipeBackEnabled(false);
    })
    .config(function(GpsProvider, SynchronizerProvider, CONFIGURATION) {
        SynchronizerProvider
        //.autoLoadSynchronizers() //Auto Load Synchronizer via Reflection
        //.frequency(15000); //Frequency between sync process

        //GPS Configuration
        GpsProvider
            .enableDeviceGPS() //Enable GPS Tracking
            .autoStart() //Auto Start
            .accuracyThreshold(65) //Real GPS Aproximaty is aprox 65, (in meters)
            .frequency(15000); //Try to get GPS Track each 15 seconds
    })
    .config(function(
        SynchronizerProvider,
        GpsProvider,
        ApplicationCleanseProvider,
        CONFIGURATION) {

        //Enable Debug for GPS and RouteTracker
        if (CONFIGURATION.debugging) {
            //Debugger Information
            SynchronizerProvider.debug();
            ApplicationCleanseProvider.debug();
            GpsProvider.debug();
        }

    })
    .config(function($ApiProvider, FileProvider, CONFIGURATION) {
        //API Base Endpoint
        var API_ENDPOINT = CONFIGURATION.API_Endpoint;
        var FILE_ENDPOINT = CONFIGURATION.API_Endpoint + "/Files/";

        $ApiProvider.setEndpoint(API_ENDPOINT);
        FileProvider.setEndpoint(FILE_ENDPOINT);
    })
    .config(function($IdentityProvider) {
        $IdentityProvider
            .enable() //Enable
            //.redirectToLoginOnLogout(false)
            .setLogInRoute("security/identity/login")
            .setIssuerEndpoint("Security/Authorize")
            .setWhiteListResolver(function(toState, current) {

                //Only Enable Access to Exception && Public State's
                if (toState.name.startsWith("boot") ||
                    toState.name.startsWith("blank.") ||
                    toState.name.startsWith("exception.") ||
                    toState.name.startsWith("public.")) {
                    return true;
                }

                //Restrict Other State's
                return false;

            });

    })
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                // ---------------------------------------------
                // APP LAYOUT (SECURED)
                // ---------------------------------------------
                templateUrl: "views/layouts/default.html",
                controller: "DefaultLayoutController"
            })
            .state('blank', {
                url: "/blank",
                abstract: true,
                // ---------------------------------------------
                // BLANK LAYOUT (EMPTY MASTER)
                // ---------------------------------------------
                templateUrl: "views/layouts/blank.html",
                controller: "BlankLayoutController"
            })
            .state('exception', {
                url: "/exception",
                abstract: true,
                // ---------------------------------------------
                // EXCEPTION TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/exception.html",
                controller: "ExceptionLayoutController"
            });

        $urlRouterProvider.otherwise(function($injector, $location) {
            if ($location.path() !== "/") {
                var $state = $injector.get("$state");
                var $log = $injector.get("$log");

                $log.error("404", $location);
                $state.go("exception.error/404");
            }
        });
    })
    .config(function($logProvider, CONFIGURATION) {
        $logProvider.debugEnabled(CONFIGURATION.debugging || false);
    });
