angular.module("config", []).constant("GLOBAL_CONFIGURATION",
{
    application:
    {
        version: "1.0.0-beta.1",
        environment: "dev",
        language: "es",
        home: "app/home"
    },

    on_build_new_version: function(newVersion, oldVersion)
    {

        //When has new Version , set the mark in the localstoage 
        localStorage.setItem("$_new_version", 1);
    },

    stamps:
    {
        personal_data: "$_personal_data",
        new_version: "$_new_version"
    }
});
