//------------------------------------------------------
// Company: Gale Framework.
// Author: dmunozgaete@gmail.com
// 
// Description: Application Bundle for Business Use
// 
// Use as you need =).
//------------------------------------------------------
angular.manifiest('custom', [
    'custom.layouts',
    'custom.controllers',
    'custom.directives',
    'custom.filters',
    'custom.services',
    'custom.components'
], [

    'toastr' //Toast Plugin for sending toast message
]);
