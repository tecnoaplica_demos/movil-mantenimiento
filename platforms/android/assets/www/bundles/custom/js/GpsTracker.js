/*------------------------------------------------------
 Company:           Gale Framework.
 Author:            David Gaete <dmunozgaete@gmail.com> (https://github.com/dmunozgaete)
 
 Description:       GPS Tracker
------------------------------------------------------*/
angular.module('custom.services')
    .service('GpsTracker', function(
        $q,
        $Api,
        Gps,
        $timeout,
        $log,
        $Identity) {

        Gps.$on("gps.update", function(position) {

            if ($Identity.isAuthenticated()) {
                var coords = position.coords;
                if (coords.latitud != 0 && coords.longitude != 0) {
                    //TRACK TO SERVER THE GEO-LOCATION
                    $Api.create("Activities/Gps/Track", {
                        accuracy: coords.accuracy,
                        altitude: coords.altitude,
                        altitudeAccuracy: coords.altitudeAccuracy,
                        latitude: coords.latitude,
                        longitude: coords.longitude,
                        speed: coords.speed
                    });
                }

            };

        });

        return self;
    })
    .run(function(GpsTracker) {
        //Auto Instance
    });
