/*------------------------------------------------------
 Company:           Gale Framework.
 Author:            David Gaete <dmunozgaete@gmail.com> (https://github.com/dmunozgaete)
 
 Description:       Notification Synchronizer
------------------------------------------------------*/
angular.module('app.services.synchronizers')
    .service('NotificationSynchronizer', function(
        $q,
        BaseEventHandler,
        $LocalStorage,
        pouchDB,
        $Api,
        $timeout,
        $Identity,
        $filter) {
        // SYNC VARIABLES
        var self = Object.create(BaseEventHandler); //Extend From EventHandler
        var database = {
            name: "notifications",
            options: {
                auto_compaction: true //Remove revision's
            }
        };
        var label = database.name + "_stamp";

        //----------------------------------------
        // CONFIGURATION STEP (LIKE CONSTRUCTOR)
        self.configure = function() {
            var defer = $q.defer();

            //------------------------------------------------
            //If stamp never exists, destroy database to reset
            var stamp = $LocalStorage.get(label);
            if (!stamp) {
                pouchDB(database.name).destroy().then(function() {

                    //RE-CREATE 
                    var db = pouchDB(database.name, database.options);

                    //CREATE VIEW'S FOR FASTER RETRIEVAL
                    var ddoc = {
                        _id: '_design/queries',
                        views: {
                            //GET ALL NOTIFICATIONS
                            all: {
                                map: function(doc) {
                                    emit(doc);
                                }.toString()
                            },

                            //GET UNREADED DOCUMENT'S
                            unreaded: {
                                map: function(doc) {
                                    if (doc.readed == false) {
                                        emit(doc);
                                    };
                                }.toString()
                            }
                        }
                    };
                    db.put(ddoc).then(function() {
                        defer.resolve();
                    }).catch(function(err) {
                        throw err;
                    });

                })
            } else {
                //Cleaning is finish :P
                defer.resolve();
            }
            //------------------------------------------------

            //--------------------------------
            //Get Current Notifications Counter when ready
            defer.promise.then(function() {
                updateCount();
            });

            return defer.promise;
        };

        //----------------------------------------
        // SYNC STEP
        self.synchronize = function() {
            var defer = $q.defer();

            //------------------------------------------------
            // ONLY WHEN IS AUTHENTICATED
            if (!$Identity.isAuthenticated()) {
                defer.resolve();
                return defer.promise;
            }
            //------------------------------------------------

            //------------------------------------------------
            var stamp = $LocalStorage.get(label);
            if (!stamp) {
                //If not exists, the first date is now- (1 months)
                stamp = moment().subtract(1, 'M').toDate().toISOString();
            }

            //----------------------------------------
            //Get New's Notification's
            $Api.read("/Notifications", {
                timestamp: stamp
            }).success(function(data) {
                if (data.items.length > 0) {
                    //SET key with the date
                    angular.forEach(data.items, function(item) {
                        item._id = item.createdAt;

                        //Decode Context
                        switch (item.type.identifier) {
                            case "INFO":
                                item.image = $filter("restricted")(item.image);
                                break;
                        }

                    });

                    //-----------------------------------------
                    //Bulk all new notification's to storage
                    var db = pouchDB(database.name);
                    db.bulkDocs(data.items).then(function() {

                        defer.resolve();

                        //Set new Stamp =)!
                        $LocalStorage.set(label, data.timestamp);


                        //Call to update the notification's counter's
                        updateCount();
                    });
                } else {
                    defer.resolve();
                }

            }).error(function() {
                defer.reject();
            });

            return defer.promise;
        };

        //-----------------------------------------
        // RESET ACTION
        self.reset = function() {
            //DROP AND RE-CREATE DB
            $LocalStorage.remove(label);
            return self.configure();
        };

        //-----------------------------------------
        // CUSTOM ACTION'S
        var updateCount = function() {
            var defer = self.getUnreadedCount();
            defer.then(function(count) {
                self.$fire("notifications.update-counter", [count]);
            });

            return defer;
        };

        self.getUnreadedCount = function() {
            var db = pouchDB(database.name);
            var defer = $q.defer();

            var promise = db.query('queries/unreaded', {
                include_docs: false
            });

            promise.then(function(res) {
                defer.resolve(res.total_rows);
            }, defer.reject);

            return defer.promise;
        };

        self.paginate = function(limit) {

            var db = pouchDB(database.name);
            var options = {
                limit: limit,
                include_docs: true,
                descending: true,
                skip: 1 //
            };

            var totalRows = 0;
            var currentRows = 0;

            var hasNext = function hasNext() {
                return totalRows > 0 && currentRows < totalRows;
            }

            var nextPage = function nextPage() {
                var defer = $q.defer();

                db.allDocs(options).then(function(res) {

                    if (res.rows.length > 0) {
                        //Set counter's
                        options.startkey = res.rows[res.rows.length - 1].key;
                        totalRows = res.total_rows - 1;
                        currentRows += res.rows.length;
                        options.skip = 1;
                    }

                    //Return Item's
                    var items = _.pluck(res.rows, 'doc');
                    defer.resolve(items);

                }, function(err) {
                    $log.error(err);
                    defer.reject(err);
                });

                return defer.promise;
            };

            var paginate = nextPage();
            paginate.nextPage = nextPage;
            paginate.hasNext = hasNext;

            return paginate;
        };


        self.markAllAsReaded = function() {
            var defer = $q.defer();
            var db = pouchDB(database.name);
            db.query('queries/unreaded', {
                include_docs: true
            }).then(function(res) {
                //Mark all unreaded item's as READED
                var items = _.pluck(res.rows, 'doc');
                angular.forEach(items, function(item) {
                    item.readed = true;
                })
                db.bulkDocs(items).then(function() {

                    var stamp = $LocalStorage.get(label);
                    $Api.update("/Notifications/MarkAsReaded", {
                        timestamp: stamp
                    }).then(defer.resolve, defer.reject);

                    updateCount();

                }, defer.reject);

            }, defer.reject);

            return defer.promise;
        };

        return self;
    });
