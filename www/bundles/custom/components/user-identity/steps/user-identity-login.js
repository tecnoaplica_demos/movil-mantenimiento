angular.module("custom.controllers")
    .directive('userIdentityLogin', function() {
        return {
            restrict: 'E',
            scope:  {
                onActionCall: '&', //Comunication Step
                canRegister: '=', //Can Register??
                canRecoverPassword: '=', //Can Recover Password??
                authenticators: '=' //Array of Availables Authenticators
            },
            templateUrl: 'bundles/custom/components/user-identity/steps/user-identity-login.tpl.html',
            controller: function($scope, $Api, $q, $Identity, $ionicLoading, $cordovaFacebook, $Configuration) {

                $scope.signature = $Configuration.get("application");
                var throwError = function(err) {
                    $ionicLoading.hide();

                    $ionicLoading.show({
                        template: 'Hubo un problema al autenticarte, intentalo denuevo...',
                        duration: 3000
                    });

                    $log.error(err);
                };

                var onAuthenticated = function() {
                    $scope.onActionCall()("AUTHENTICATED");
                    $ionicLoading.hide(); //Hide Loading
                };

                var customLogin = function(credentials) {
                    $Identity.authenticate(credentials)
                        .success(function(data) {
                            onAuthenticated();
                        })
                        .error(throwError);
                };

                var facebookLogin = function() {
                    $cordovaFacebook.login(["public_profile", "email"])
                        .then(function(data) {
                            var accessToken = data.authResponse.accessToken;
                            if (data.status == "connected") {
                                $ionicLoading.show({
                                    template: "Obteniendo Información"
                                });

                                $cordovaFacebook.api("me/?fields=id,email,name,picture.type(large),location", ["public_profile"])
                                    .then(function(data) {

                                        $ionicLoading.show({
                                            template: "Autorizando...",
                                            hideOnStateChange: true
                                        });

                                        //GO TO API, CHECK THE TOKEN ,
                                        // AND IF IS CORRECT , CREATE OR GET THE USER
                                        data.accessToken = accessToken;
                                        data.image = data.picture.data.url;

                                        delete data.picture; //Remove this =)

                                        $Api.create("/Security/Oauth/Facebook", data)
                                            .success(function(oauthToken, status) {

                                                if (status == 201) {

                                                }


                                                $Identity.logIn(oauthToken);
                                                onAuthenticated();

                                            }).error(throwError);

                                    }, throwError);
                            } else {
                                throwError("NOT_CONNECTED");
                            }
                        }, function(e) {
                            throwError("USER_CANCEL");
                        });
                };

                $scope.close = function() {
                    $scope.onClose();
                };

                $scope.hasAuthenticator = function(authenticator) {
                    return _.indexOf($scope.authenticators, authenticator) >= 0;
                };

                //Authentication Step
                $scope.authenticate = function(authenticator, credentials) {

                    $ionicLoading.show({
                        template: 'Identificando...'
                    });

                    try {
                        switch (authenticator) {
                            case 'facebook':
                                facebookLogin();
                                break;
                            case 'custom':
                                customLogin(credentials);
                                break;
                        }
                    } catch (err) {
                        $ionicLoading.hide();

                        $ionicLoading.show({
                            template: 'No se encuentra disponible el inicio de sesión para {0} en estos momentos...'.format([authenticator]),
                            duration: 3000
                        });

                        $log.error(err);
                    }
                };
            }
        };
    });
