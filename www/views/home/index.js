angular.route('app.home/index', function(
    $scope,
    $state,
    $log,
    $Api,
    $timeout,
    $Identity,
    $q
) {
    //------------------------------------------------
    // Model
    $scope.model = {};

    var defers = [
        $Api.read("Activities/Resume")
    ];

    //------------------------------------------------    
    $q.all(defers).then(function(resolves) {
        var resume = resolves[0];

        var chart = {
            labels: [],
            data: [],
            colours: []
        };

        angular.forEach(resume.statistics, function(item) {
            chart.labels.push(item.label);
            chart.data.push(item.value);
            chart.colours.push(item.color);
        });

        //Set Model
        $scope.model.chart = {
            labels: chart.labels,
            data: chart.data,
            colours: chart.colours,
            options: {
                percentageInnerCutout: 60,
                segmentStrokeWidth: 3,
                responsive: false,
                maintainAspectRatio: false
            }
        };
        $scope.model.components = resume.components;

        $log.debug(resume.components);
    });

    //------------------------------------------------
    // Action's
    $scope.details = function(item) {
        $state.go("app.components/view/index", {
            component: item.token
        });
    };


});
