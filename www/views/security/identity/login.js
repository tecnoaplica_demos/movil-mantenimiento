angular.route('security/identity/login/:redirect?', function(
    $log,
    $Configuration,
    $location,
    $scope,
    $stateParams,
    $ionicHistory,
    $cordovaSplashscreen
) {
    // Hook when the user is authenticated
    $scope.onAuthenticated = function() {
        var redirectRoute = ($stateParams.redirect||$Configuration.get("application").home);
        $location.url(redirectRoute);
    };

    // Hook when user cancel
    $scope.onCancel = function() {
        $ionicHistory.goBack();
    };

});
